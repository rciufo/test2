.. _main:

====
Main
====

These are the options that go in **[Main]** section of the config.ini file.

**run_type**: The type of run to perform

    default: ``trainFF``

    options:

        ``trainFF``: Train a force field


**epochs**: Takes an integer number for the number of training epochs

    default: ``10000``

**trajectory_file**: The ase trajectory file containing the training images.

    default: ``train.traj``

**energy_training**: Trains force field using energy of the training images. 

    default: ``True``

**force_training**: Trains force field using forces of the training images.

    default: ``True``



