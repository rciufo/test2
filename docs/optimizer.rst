.. _optimizer:

====
Optimizer
====

These are the options that go in **[Optimizer]** section of the config.ini file.

**optimizer_type**: The optimizer to be used for the machine learning.

    default: ``LBFGS``

    options:

        ``LBFGS``: L-BFGS

        ``ADAM``: ADAM


**convergence_criteria**: Convergence criteria for optimization.

    default: ``0.02``



