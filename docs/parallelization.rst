.. _parralelization:

====
Parralelization
====

These are the options that go in **[Parallelization]** section of the config.ini file.

**number_process**: Takes an integer as the number of parallel jobs to perform.

    default: ``1``

