.. _debug:

====
Debug
====

These are the options that go in **[Debug]** section of the config.ini file.

**init_model_paras**: The model paras.

    default: ``False``


