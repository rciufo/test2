.. test2_site documentation master file, created by
   sphinx-quickstart on Sat Jun  6 14:52:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to test2_site's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   main
   machinelearningmodel
   optimizer
   parallelization
   lossfunction
   fingerprints
   debug


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
