.. _lossfunction:

====
LossFunction
====

These are the options that go in **[LossFunction]** section of the config.ini file.

**loss_type**: The type of loss function used during fitting.

    default: ``rmse``

    options:

        ``rmse``: Root mean-squared error


**energy_coefficient**: The weight for the energy term of the loss function.

    default: ``1.0``

**force_coefficient**: The weight for the force term of the loss function.

    default: ``0.02``


